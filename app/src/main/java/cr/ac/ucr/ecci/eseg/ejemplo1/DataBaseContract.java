package cr.ac.ucr.ecci.eseg.ejemplo1;

import android.provider.BaseColumns;

public final class DataBaseContract {
    private DataBaseContract() {}

    public static class DataBaseEntry implements BaseColumns {

        public static final String TABLE_NAME_TABLETOP = "TableTop";
        public static final String COLUMN_NAME_ID = "ID";
        public static final String COLUMN_NAME_NAME = "Name";
        public static final String COLUMN_NAME_YEAR = "Year";
        public static final String COLUMN_NAME_PUBLISHER = "Publisher";
        public static final String COLUMN_NAME_COUNTRY = "Country";
        public static final String COLUMN_NAME_LATITUDE = "Latitude";
        public static final String COLUMN_NAME_LONGITUDE = "Longitude";
        public static final String COLUMN_NAME_DESCRIPTION = "Description";
        public static final String COLUMN_NAME_NO_PLAYERS = "No_Players";
        public static final String COLUMN_NAME_AGES = "Ages";
        public static final String COLUMN_NAME_PLAYING_TIME = "Playing_time";
    }

    private static final String TEXT_TYPE = " TEXT";
    private static final String INTEGER_TYPE = " INTEGER";
    private static final String REAL_TYPE = " REAL";
    private static final String COMMA_SEP = ",";

    public static final String SQL_CREATE_TABLETOP =
            "CREATE TABLE " + DataBaseEntry.TABLE_NAME_TABLETOP + " (" +
                    DataBaseEntry.COLUMN_NAME_ID + TEXT_TYPE + "PRIMARY KEY," +
                    DataBaseEntry.COLUMN_NAME_NAME + TEXT_TYPE + COMMA_SEP +
                    DataBaseEntry.COLUMN_NAME_YEAR + TEXT_TYPE + COMMA_SEP +
                    DataBaseEntry.COLUMN_NAME_PUBLISHER + TEXT_TYPE + COMMA_SEP +
                    DataBaseEntry.COLUMN_NAME_COUNTRY + TEXT_TYPE + COMMA_SEP +
                    DataBaseEntry.COLUMN_NAME_LATITUDE + TEXT_TYPE + COMMA_SEP +
                    DataBaseEntry.COLUMN_NAME_LONGITUDE + TEXT_TYPE + COMMA_SEP +
                    DataBaseEntry.COLUMN_NAME_DESCRIPTION+ TEXT_TYPE + COMMA_SEP +
                    DataBaseEntry.COLUMN_NAME_NO_PLAYERS + TEXT_TYPE + COMMA_SEP +
                    DataBaseEntry.COLUMN_NAME_AGES + TEXT_TYPE + COMMA_SEP +
                    DataBaseEntry.COLUMN_NAME_PLAYING_TIME + TEXT_TYPE + " )";

    public static final String SQL_DELETE_TABLETOP =
            "DROP TABLE IF EXISTS " + DataBaseEntry.TABLE_NAME_TABLETOP;

}
