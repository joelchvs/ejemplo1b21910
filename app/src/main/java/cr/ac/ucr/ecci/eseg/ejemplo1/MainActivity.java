package cr.ac.ucr.ecci.eseg.ejemplo1;


import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    public final static String EXTRA_MESSAGE = "persona";
    private static final int SECOND_ACTIVITY_RESULT_CODE = 0;


    Integer[] imgid={
            R.drawable.catan,
            R.drawable.monopoly,
            R.drawable.eldritch,
            R.drawable.mtg,
            R.drawable.hanabi
    };

    private List<TableTop> listaTableTops= new ArrayList<TableTop>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //no sé si es solo en mi celular pero siempre quedaba la base de datos en el teléfono,
        // entonces tenía que borrar cada vez que iniciaba la app
        limpiarBase();

        //insertamos los datos en la BDD
        insertarDatos();

        //Creamos la lista de objetos TableTop extrayendo los datos de la BDD
        crearListaDeTableTops();



        //Este pedazo de código es para usal un custom adapter, no me funcionó
//       CustomListAdapter adapter = new CustomListAdapter(this,imgid, listaTableTops);
//        ListView list = (ListView) findViewById(R.id.list);
//        list.setAdapter(adapter);

        //Usamos el adaptador por defecto para poblar la lista de juegos
       ArrayAdapter<TableTop> adapter = new ArrayAdapter<TableTop>(this,
               android.R.layout.simple_list_item_1,android.R.id.text1,listaTableTops);
       final ListView listView = (ListView) findViewById(R.id.list);
       listView.setAdapter(adapter);

       //método para ser redirgiidos al activity de un juego individual al tocar uno de los itemes de la lista
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // ListView Clicked item index
                int itemPosition = position;
                // ListView Clicked item value
                TableTop item = (TableTop) listView.getItemAtPosition(position);
                irJuego(item);

            }
        });
    }

    private void irJuego(TableTop tableTop){
        Intent intent = new Intent(this, JuegoActivity.class);
        intent.putExtra(EXTRA_MESSAGE,tableTop);
        startActivityForResult(intent,SECOND_ACTIVITY_RESULT_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // check that it is the SecondActivity with an OK result
        if (requestCode == SECOND_ACTIVITY_RESULT_CODE) {
            if (resultCode == RESULT_OK) {
                // Obtener datos del Intent
                String returnString = data.getStringExtra("montoStr");
                // mostrar la respuesta
                Toast.makeText(getApplicationContext(), returnString, Toast.LENGTH_LONG).show();
            }
        }
    }

    //se añade uno a uno los juegos de mesa
    public void insertarDatos(){
        insertarTupla("TT001 ","Catan","1995","Kosmos","Germany",
                "48.774538","9.188467","Picture yourself in the era of discoveries:\n" +
                        "after a long voyage of great deprivation,\n" +
                        "your ships have finally reached the coast of\n" +
                        "an uncharted island. Its name shall be Catan!\n" +
                        "But you are not the only discoverer. Other\n" +
                        "fearless seafarers have also landed on the\n" +
                        "shores of Catan: the race to settle the\n" +
                        "island has begun!","3-4","10+","1-2 hours");

        insertarTupla("TT002","Monopoly","1935","Hasbro","United States",
                "41.883736","-71.352259", "The thrill of bankrupting an opponent, but it\n" +
                        "pays to play nice, because fortunes could\n" +
                        "change with the roll of the dice. Experience\n" +
                        "the ups and downs by collecting property\n" +
                        "colors sets to build houses, and maybe even\n" +
                        "upgrading to a hotel!","2-8","8+","20-180 minutes");

        insertarTupla("TT003","Eldritch Horror ","2013","Fantasy Flight Games","United States",
                "45.015417","-93.183995", "An ancient evil is stirring. You are part of\n" +
                        "a team of unlikely heroes engaged in an\n" +
                        "international struggle to stop the gathering\n" +
                        "darkness. To do so, you’ll have to defeat\n" +
                        "foul monsters, travel to Other Worlds, and\n" +
                        "solve obscure mysteries surrounding this\n" +
                        "unspeakable horror.","1-8","14+","2-4 hours");

        insertarTupla("TT004","Magic: the Gathering","1993","Hasbro","United States",
                "41.883736","-71.352259", "Magic: The Gathering is a collectible and\n" +
                        "digital collectible card game created by\n" +
                        "Richard Garfield. Each game of Magic\n" +
                        "represents a battle between wizards known as\n" +
                        "planeswalkers who cast spells, use artifacts,\n" +
                        "and summon creatures.","2+","13+","Varies");

        insertarTupla("TT005 ","Hanabi","2010","Asmodee","France",
                "48.761629","2.065296", "Hanabi—named for the Japanese word for\n" +
                        "\"fireworks\"—is a cooperative game in which\n" +
                        "players try to create the perfect fireworks\n" +
                        "show by placing the cards on the table in the\n" +
                        "right order.","2-5","8+","25 minutes");
    }

    //método que ingresa solo una tupla
    public void insertarTupla ( String ID, String Name, String Year, String Publisher,String Country,
            String Latitude, String Longitude, String Description, String No_Players, String Ages, String Playing_time)
    {
        TableTop test = new TableTop(ID,Name,Year,Publisher,Country,Latitude,Longitude,
                Description,No_Players,Ages,Playing_time);
        long newRowId = test.insertar(getApplicationContext());
    }

    //método para limpar las tablas
    public void limpiarBase()
    {
        DataBaseHelper dataBaseHelper = new DataBaseHelper(getApplicationContext());
        SQLiteDatabase db = dataBaseHelper.getWritableDatabase();
        db.execSQL("delete from TABLETOP");
    }

    //método que obtiene datos de la BDD y crea una lista de TableTop
    public void crearListaDeTableTops()
    {
        DataBaseHelper dataBaseHelper = new DataBaseHelper(getApplicationContext());
        SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery("select * from TABLETOP",null);

        if (cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                        this.listaTableTops.add(new TableTop(cursor.getString(0),cursor.getString(1),
                                cursor.getString(2),cursor.getString(3),cursor.getString(4),
                                cursor.getString(5),cursor.getString(6),cursor.getString(7),
                                cursor.getString(8),cursor.getString(9),cursor.getString(10)));
                cursor.moveToNext();
            }
        }
    }

    }
