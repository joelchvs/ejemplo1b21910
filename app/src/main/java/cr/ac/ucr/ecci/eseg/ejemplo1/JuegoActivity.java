package cr.ac.ucr.ecci.eseg.ejemplo1;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class JuegoActivity extends AppCompatActivity {

    public TextView textview1;
    public TextView textview2;
    public TextView textview3;
    public TextView textview4;
    public TextView textview5;
    public TextView textview8;
    public TextView textview9;
    public TextView textview10;
    public TextView textview11;
    public Button buttonAbrirEnMapas;
    private TableTop tableTop;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_juego);

        // recibimos el Intent y los parámetros
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            // obtenemos el objeto TableTop
            tableTop = bundle.getParcelable(MainActivity.EXTRA_MESSAGE);
        }

        //instaciamos los elementos del layout para poder modificarlo
        buttonAbrirEnMapas = (Button) findViewById(R.id.buttonAbrirEnMapas);
        buttonAbrirEnMapas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                irMapa(tableTop.getLatitude(),tableTop.getLongitude());
            }
        });
        textview1 = (TextView)findViewById(R.id.textView1);
        textview2 = (TextView)findViewById(R.id.textView2);
        textview3 = (TextView)findViewById(R.id.textView3);
        textview4 = (TextView)findViewById(R.id.textView4);
        textview5 = (TextView)findViewById(R.id.textView5);
        textview8 = (TextView)findViewById(R.id.textView8);
        textview9 = (TextView)findViewById(R.id.textView9);
        textview10 = (TextView)findViewById(R.id.textView10);
        textview11 = (TextView)findViewById(R.id.textView11);

        //Acá ponemos los datos del juego en el layout
        textview1.setText("ID Code: \n"+tableTop.getID());
        textview2.setText("Name: \n"+tableTop.getName());
        textview3.setText("Year: \n"+tableTop.getYear());
        textview4.setText("Publisher: \n"+tableTop.getPublisher());
        textview5.setText("Country: \n"+tableTop.getCountry());
        textview8.setText("Description: \n"+tableTop.getDescription());
        textview9.setText("Number of players: \n"+tableTop.getNo_Players());
        textview10.setText("Ages: \n"+tableTop.getAges());
        textview11.setText("Playing time: \n"+tableTop.getPlaying_time());
    }

    //método que nos redirige a google maps y coloca un marcador en la direccion correspondiente
    private void irMapa(String latitud, String longitud)
    {
        String url = "geo:" + latitud + "," +longitud;
        String q = "?q="+ latitud + "," +
                longitud + "(" + "location" + ")";
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url + q));
        intent.setPackage("com.google.android.apps.maps");
        startActivity(intent);
    }

}
