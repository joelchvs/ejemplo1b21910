package cr.ac.ucr.ecci.eseg.ejemplo1;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;
import android.os.Parcelable;

public class TableTop implements Parcelable {

    private final String ID;
    private final String Name;
    private final String  Year;
    private final String Publisher;
    private final String Country;
    private final String Latitude;
    private final String Longitude;
    private final String Description;
    private final String No_Players;
    private final String Ages;
    private final String Playing_time;


    public TableTop(String ID, String name, String year, String publisher, String country, String latitude, String longitude, String description, String no_Players, String ages, String playing_time) {
        this.ID = ID;
        this.Name = name;
        this.Year = year;
        this.Publisher = publisher;
        this.Country = country;
        this.Latitude = latitude;
        this.Longitude = longitude;
        this.Description = description;
        this.No_Players = no_Players;
        this.Ages = ages;
        this.Playing_time = playing_time;
    }

    protected TableTop(Parcel in) {
        ID= in.readString();
        Name= in.readString();
        Year= in.readString();
        Publisher= in.readString();
        Country= in.readString();
        Latitude= in.readString();
        Longitude= in.readString();
        Description= in.readString();
        No_Players= in.readString();
        Ages= in.readString();
        Playing_time= in.readString();

    }

    public long insertar(Context context)
    {
        // usar la clase DataBaseHelper para realizar la operacion de insertar
        DataBaseHelper dataBaseHelper = new DataBaseHelper(context);
        // Obtiene la base de datos en modo escritura
        SQLiteDatabase db = dataBaseHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_ID,getID());
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_NAME,getName());
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_YEAR,getYear());
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_PUBLISHER,getPublisher());
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_COUNTRY,getCountry());
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_LATITUDE,getLatitude());
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_LONGITUDE,getLongitude());
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_DESCRIPTION,getDescription());
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_NO_PLAYERS,getNo_Players());
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_AGES, getAges());
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_PLAYING_TIME,getPlaying_time());

        return db.insert(DataBaseContract.DataBaseEntry.TABLE_NAME_TABLETOP, null,
                values);

    }
    public static final Creator<TableTop> CREATOR = new Creator<TableTop>() {
        @Override
        public TableTop createFromParcel(Parcel in) {
            return new TableTop(in);
        }

        @Override
        public TableTop[] newArray(int size) {
            return new TableTop[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(ID);
        parcel.writeString(Name);
        parcel.writeString(Year);
        parcel.writeString(Publisher);
        parcel.writeString(Country);
        parcel.writeString(Latitude);
        parcel.writeString( Longitude);
        parcel.writeString( Description);
        parcel.writeString( No_Players);
        parcel.writeString(Ages);
        parcel.writeString(Playing_time);
    }

    public String getID() {
        return ID;
    }

    public String getName() {
        return Name;
    }

    public String getYear() {
        return Year;
    }

    public String getPublisher() {
        return Publisher;
    }

    public String getCountry() {
        return Country;
    }

    public String getLatitude() {
        return Latitude;
    }

    public String getLongitude() {
        return Longitude;
    }


    public String getDescription() {
        return Description;
    }

    public String getNo_Players() {
        return No_Players;
    }

    public String getAges() {
        return Ages;
    }

    public String getPlaying_time() {
        return Playing_time;
    }

    // Para el ListView
    @Override
    public String toString() {
        String descrpcion = this.Name+"\n \n"+this.Description;
        return descrpcion;
    }
}
