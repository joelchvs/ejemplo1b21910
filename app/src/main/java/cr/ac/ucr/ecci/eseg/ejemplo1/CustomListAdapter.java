package cr.ac.ucr.ecci.eseg.ejemplo1;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class CustomListAdapter extends ArrayAdapter<TableTop> {
    private final Activity context;

    private final Integer[] imgid;
    private List<TableTop> listaTableTops;


    public CustomListAdapter(Activity context, Integer[] imgid, List<TableTop> listaTableTops) {
        super(context, R.layout.mi_lista_personalizada);
        this.context = context;
        this.listaTableTops=listaTableTops;
        this.imgid = imgid;
    }
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.mi_lista_personalizada, null, true);
        TextView nombre = (TextView) rowView.findViewById(R.id.name);
        ImageView imagen = (ImageView) rowView.findViewById(R.id.icon);
        TextView descripcion = (TextView) rowView.findViewById(R.id.description);
        nombre.setText(listaTableTops.get(position).getName());
        imagen.setImageResource(imgid[position]);
        descripcion.setText(listaTableTops.get(position).getDescription());
        return rowView;
    }
}